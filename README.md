# README #

Khemlabs - Kickstarter

### What is this repository for? ###

This Kickstarter is intended for developers that want to create rapidly and easily
nodejs backend projects.

### Features ###

> NodeJS server

> Authentication (JWT and PASSPORT)

> Facebook login

> API endpoints generation from models (epilogue).

> Database management (sequelize).

> Structuring project files

> Docker debugging

### Requirements ###

> Docker  > 1.9

> Vagrant > 1.8.1

> [nodejs | bash] The kickstarter has commands that can be run with nodejs or bash

### Lifting a kickstarter on dev environment ###

#### Easy way ####

This will lift a kickstarter environment with client and API on the same
docker container on PORT 3000 linked to a postgres database container.

```
#!bash
vagrant up --no-parallel
```

#### Separated API ####

There's lot of times that your API is not on the same container that your client, 
for that cases you can set Environment Variables for vagrant with 
[ALLOW_ORIGIN=[VARIABLE_VALUE]] vagrant up --no-parallel

#### Example ####

```
#!bash
ALLOW_ORIGIN='http://client.test.com' vagrant up --no-parallel
```

#### Vagrant environment variables ####

```
#!bash

PORT                  - Default: 3000
CONTAINER_NAME        - Default: testing-app
ALLOW_ORIGIN          - Default: '' // comma separated values
DB_CONTAINER          - Default: testing-db
DATABASE_USER         - Default: postgres
DATABASE_PASSWORD     - Default: postgres
```

### RUN kickstarter on PROD environment ###

Vagrant is only used to simplify development, on production docker commands must be run by the user.

Three thing must happen before having a running kickstarter on production.

1) Lift database container
2) Build kickstarter image
3) Run kickstarter container

#### Example ####

```
#!bash
1) Lift database container

docker run -d --name DB_CONTAINER_NAME -e POSTGRES_USER=DB_USER -e POSTGRES_PASSWORD=DB_PASSWORD postgres

If you have your data mapped

-e PGDATA=HOST_PATH
```
```
#!bash
2) Build kickstart image

docker build -t IMAGE_NAME .
```
```
#!bash
3) Run kickstart container

docker run -d --name CONTAINER_NAME -e DATABASE_NAME=DB_NAME -e DATABASE_USER=DB_USER -e DATABASE_PASSWORD=DB_PASSWORD --link DB_CONTAINER_NAME:db IMAGE_NAME

If you are using nginx proxy remember to add
-e VIRTUAL_HOST=HOST

If you are using letsencrypt remember to add
-e LETSENCRYPT_HOST=HOST -e LETSENCRYPT_EMAIL=EMAIL

If you have your data mapped remember to add
-v HOST_PATH:/data
```

```
#!bash
docker build -t testing --build-arg ALLOW_ORIGIN='http://client.test.com' .
```

#### Environment Variables ####

Environment variables are added to the docker run command with -e or --env

```
#!bash
DATABASE_NAME         - Default: postgres
DATABASE_USER         - Default: postgres
DATABASE_PASSWORD     - Default: postgres
DATABASE_DIALECT      - Default: postgres
DATABASE_HOST         - Default: 5432
DATABASE_LOGGING      - Default: false
ADMIN_USERNAME        - Default: admin
ADMIN_FIRSTNAME       - Default: admin
ADMIN_LASTNAME        - Default: admin
ADMIN_PASSWORD        - Default: admin
ADMIN_EMAIL           - Default: dev@khemlabs.com
PATH_IMG              - Default: /data/images/
PATH_LOGO             - Default: public/assets/images/logos/khemlabslogomenu.png
USER_ROLES            - Default: 'admin,superadmin,readonly'
ADMIN_ROLES           - Default: 'admin,superadmin'
ADMIN_ROLES_TEST      - Default: 'admintest,superadmintest'
USE_FACEBOOK_LOGIN    - Default: false
USE_DATABASE          - Default: true
PORT                  - Default: 3000
ALLOW_ORIGIN          - Default: '' // comma separated values
FACEBOOK_API_KEY
FACEBOOK_API_SECRET
FACEBOOK_CALLBACK_URL
```

### Global Variables ###

> \__base           => BASE PATH (\__dirname + '/')

> \__configuration  => CONFIGURATION PATH

> \__middlewares    => MIDDLEWARES PATH

> \__models         => MODELS PATH

> \__routes         => ROUTES PATHS

> \__services       => SERVICES PATHS

> \__websockets     => WEBSOCKETS PATHS

> \__config         => CONFIG OBJECT

> \__log            = LOG OBJECT

### Striptags validation ###

By default the kickstarter removes all html tags off all inputs. If you want to add a whitelist, just add the array allowedTags to the column definition in your model.

### Send Emails ### 

Add "khem-mail": "git+https://bitbucket.com/khemlabs/khem-mail.git#0.1.0" to your package json

Docs at https://bitbucket.com/khemlabs/khem-mail.git

### Debugging ###

```
#!bash
node lib/debug.js OR bash lib/debug.sh

http://127.0.0.1:9080/?ws=127.0.0.1:9080&port=5858
```

### Directories ###

The kickstarter divides the client and the server. The client is a functional angular application and can be allocated in a different server of the API. The server is a restful API that serves all the endpoints needed by the client.

The directories are devided in:

> lib (kickstarter files)

> server (API files)

__Warning: YOU SHOULD NOT CREATE NEW DIRECTORIES, IF YOU DO, BE AWARE OF ADDING THEM TO GULP FILE.__

__API (server application)__
```
#!bash

Is allocated in the server folder.
```

### EPILOGUE endpoints authorization ###

All the endpoints can validate login and permission, to do so you can define the parameter (authenticate) in the epilogue method of the model

__Example in server/models/sequelize/Testing.example.js__

### EPILOGUE endpoints middlewares ###

Epilogue has the ability to receive middlewares, if you want to use them you can create them in the directory server/middlewares/epilogue

__Example in server/middlewares/epilogue/Example.middleware.js__

### Automatic reloading ###

If you use vagrant in your development environment it will automatically reload node js if changes are detected in the directory __(server/*)__

### Known Issues ###

#### npm install not working ####

This is not entirly true, bower and npm install is working properly, the problem is vagrant.

Because vagrant is mapping our work directory for automatic reload on file changes, the original
docker directory is overwriten with the content of the host machine.

Partial Solution

vagrant up --no-parallel (will execute bower and npm install on host machine) after docker container is lifted.

### Khemlabs kickstarter default libs ###

> khem-auth

> khem-cross-origin

> khem-epilogue

> khem-image-upload

> khem-log

> khem-sequelize-orm

### Khemlabs kickstarter optional libs ###

#### khem-socket ####

> npm install git+https://bitbucket.com/khemlabs/khem-socket.git --save

> DOCS at https://bitbucket.com/khemlabs/khem-socket

### Who do I talk to? ###

* dnangelus repo owner and admin
* developer elgambet and khemlabs