var async = require('async');

var csvParser = function() {
  
  var self = this;
    
  // Columns must exist
  this.columns = {
    User:         ['username', 'first_name', 'last_name']
  };
  
  this.user = function (data, columnsIdxs) {
    return {
      user: {
        username:   data[columnsIdxs['username']],
        first_name: data[columnsIdxs['first_name']],
        last_name:  data[columnsIdxs['last_name']],
        email:      data[columnsIdxs['email']],
        type:       data[columnsIdxs['type']]     || 'readonly',
        area:       data[columnsIdxs['area']]     || 'administration',
        password:   data[columnsIdxs['password']] || '12345678',
        status:     data[columnsIdxs['status']]   || 'isactive'
      }
    };
  };
  
  this.arrayWalk = function(data, columnsIdxs, entityname, assocname, assocnamepl, column_condition, value_condition){ 
    var elements    = [];
    var indexes     = [];
    var description = columnsIdxs[entityname];
    var association = columnsIdxs[assocname];
    data.forEach(function(element){
      // Check if is first time or if it has to check a parent entity
      if(element[description] !== false && indexes.indexOf(element[description]) == -1 && ((!column_condition && column_condition != 0) || element[column_condition] == value_condition)){
        indexes.push(element[description]);
        var insert = {};
        for(var column in columnsIdxs){
          if(column != entityname)
            insert[column] = element[columnsIdxs[column]];
        }
        insert.description = element[description];
        if(element[association]){
          insert[assocnamepl] = self[assocname](data, columnsIdxs, description, element[description]);
        }
        elements.push(insert);
      }
    });
    return elements;
  };
  
  /*
  // EXAMPLE OF ARRAY-WALK
  this.modelname = function (data, columnsIdxs, column_condition, value_condition) {
    return self.arrayWalk(
      data, 
      columnsIdxs, 
      'modelname', 
      'relatedmodel', 
      'relatedmodels', 
      'related_model_column', 
      'related_model_column_value'
    );
  };
  //*/
  
  /**
  * Checks if all required columns exists
  * @param {array} columns
  * @param {function} masterCallback
  */
  this.checkColumns = function(type, columns, masterCallback) {
    var columnsNotFound = [];
    async.each(self.columns[type], function (column, callback) {
        if((columns.indexOf(column) >= 0)) return callback();
        else {
          columnsNotFound.push(column);
          return callback('not_found');
        }
      }, function(err) {
        if( err ) {
          var msg = '(' + JSON.stringify(columnsNotFound) + ') ' + JSON.stringify(err);
          __log.error(msg, 'services/csvParser', 'checkColumns');
          return masterCallback(false, columnsNotFound);
        } else {
          return masterCallback(true);
        }
      });
  };
  
}

module.exports = new csvParser();