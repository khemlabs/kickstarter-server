/**
* This method is used by middlewares to get resultset for autocomplete methods
* 
* @param model {object} Sequelize model
* @param model_id {string} Column primary key name
* @param model_key {string} Column to search name
* @param status {[object | false]} If "false" the search will not filter by status else if 
*                                  "object" query will filter by query[status.column] = status.value 
*                                  unless disabled = true is recived
* @param attributes {object} List of columns of resulset
* @param data {object} req object recived
*                      req.params = {
*                         [q], 
*                         [disabled], 
*                         [notIn: {column, value}],
*                         [exclude]
*                      }
* @param include {object} sequelize object include
*/
exports.get = function(model, model_id, model_key, status, attributes, data, include){
  return new Promise(function (resolve, reject) {
    if(data.query && data.query.autocomplete){
      var search = {
        include: include,
        attributes: attributes,
        where: {}
      };
      if(data.query.q) search.where[model_key] = {ilike: '%' + data.query.q + '%'};
      if(!data.query.disabled && status) search.where[status.column] = status.value;
      if(data.query.notIn) search.where[data.query.notIn.column] = {'notIn': data.query.notIn.value};
      if(data.query.exclude) {
        if(typeof data.query.exclude == 'string') data.query.exclude = [data.query.exclude];
        search.where[model_id] = {'notIn': data.query.exclude};
      }
      model.findAll(search).then(function(results){
        return resolve(results);
      }, function(err){
        __log.error(err, 'services/autocompleteService', 'get');
        return reject();
      });
    } else {
      return reject();
    }
  });
};
