var orm   = require('khem-sequelize-orm'),
    async = require('async');

var databaseInitialization = function() {
  var self = this;
  
  this.admin = {
    username:   __config.ADMIN_USERNAME  || 'admin',
    first_name: __config.ADMIN_FIRSTNAME || 'admin',
    last_name:  __config.ADMIN_LASTNAME  || 'admin',
    password:   __config.ADMIN_PASSWORD  || 'admin',
    email:      __config.ADMIN_EMAIL     || 'dev@khemlabs.com',
    type:       'superadmin',
    area:       'administration',
    status:     'isactive',
    role_id:     1
  };
  
  this.user = {
    username:   __config.READONLY_USERNAME  || 'readonly',
    first_name: __config.READONLY_FIRSTNAME || 'readonly',
    last_name:  __config.READONLY_LASTNAME  || 'readonly',
    password:   __config.READONLY_PASSWORD  || 'readonly',
    email:      __config.READONLY_EMAIL     || 'dev@khemlabs.com',
    type:       'readonly',
    area:       'administration',
    status:     'isactive',
    role_id:     3
  };
  
  this.initializeRoles = function(callback){
    console.info('[db initialization] Validating/Creating roles');
    if(!__config.ROLES || !__config.ROLES.length || __config.ROLES.length < 3){
      __log.error('__config.ROLES must exist', 'services/databaseInitialization', 'initializeRoles');
    } else {
      var error = false;
      var found = {superadmin: false, admin: false, readonly: false};
      __config.ROLES.forEach(function(role){
        if(!role.name) error = true;
        else if(role.name == 'superadmin')  found.superadmin = true;
        else if(role.name == 'admin')       found.admin = true;
        else if(role.name == 'readonly')    found.readonly = true;
        if(!role.rules) error = true;
        else {
          try{
            JSON.stringify(role);
          } catch(err){
            error = true;
          }
        }
      });
      if(error || !found.superadmin || !found.admin || !found.readonly){
        if(error) __log.error('__config.ROLES bad syntax', 'services/databaseInitialization', 'initializeRoles');
        if(!found.superadmin) __log.error('__config.ROLES without superadmin', 'services/databaseInitialization', 'initializeRoles');
        if(!found.admin) __log.error('__config.ROLES without admin', 'services/databaseInitialization', 'initializeRoles');
        if(!found.readonly) __log.error('__config.ROLES without readonly', 'services/databaseInitialization', 'initializeRoles');
      } else {
        if(__config.ROLES[0].name != 'superadmin' || __config.ROLES[1].name != 'admin' || __config.ROLES[2].name != 'readonly'){
            __log.error('__config.ROLES with admin, superadmin or readonly on wrong index', 'services/databaseInitialization', 'initializeRoles');
        } else {
          async.eachSeries(__config.ROLES, function(role, next){
            orm.models.Role.findOrCreate({where: {name: role.name}, defaults: role}).then(function(rrole){
              return next();
            }).catch(orm.sequelize.ValidationError, function (err) {
              return next(err);
            }).catch(function (err) {
              return next(err);
            });
          }, function(err){
            if(err){
              __log.error(err, 'services/databaseInitialization', 'initializeRoles');
              return callback(err);
            } else {
              return callback();  
            }
          });  
        }
      }
    }
  };
  
  this.initializeReadOnly = function(callback){
    console.info('[db initialization] Validating/Creating readonly user');
    orm.models.User.findOrCreate({
      where: {username: self.user.username}, 
      defaults: self.user
    }).then(function(user){
      return callback();
    }).catch(orm.sequelize.ValidationError, function (err) {
      if(err) __log.error(err, 'services/databaseInitialization', 'initializeReadOnly');
      return callback(err);
    }).catch(function (err) {
      if(err) __log.error(err, 'services/databaseInitialization', 'initializeReadOnly');
      return callback(err);
    });
  };
  
  this.initializeAdmin = function(callback){
    console.info('[db initialization] Validating/Creating superadmin user');
    orm.models.User.findOrCreate({
      where: {username: self.admin.username}, 
      defaults: self.admin
    }).then(function(user){
      return callback();
    }).catch(orm.sequelize.ValidationError, function (err) {
      if(err) __log.error(err, 'services/databaseInitialization', 'initializeAdmin');
      return callback(err);
    }).catch(function (err) {
      if(err) __log.error(err, 'services/databaseInitialization', 'initializeAdmin');
      return callback(err);
    });
  };
  
  this.init = function(){
    return new Promise(function (resolve, reject) {
      self.initializeRoles(function(err){
        if(!err){
          self.initializeAdmin(function(err){
            if(!err){
              self.initializeReadOnly(function(err){
                if(!err){
                  return resolve();
                } else {
                  return reject();
                }
              });
            } else {
              return reject();
            }
          });  
        } else {
          return reject();  
        }
      });
    });
    
  };
};

module.exports = new databaseInitialization();
