var csv = require('csv'),
    async = require('async'),
    orm = require('khem-sequelize-orm'),
    csvParser = require(__services + 'csvParser');

/**
* All import methods are include here
*/
var csvImport = function() {
  var self = this;
  
  this.users = function(csvData, user, callback) {
    return self.importData(csvData, user, 'User', callback);
  };
  
  this.import = function(config){
    // Execute functions in order, execute the next function when previous finished
    async.eachSeries(config.data, function(element, next) {
      config.model.import(config.parseFunction(element, config.columnsIndex), function(err){
        return (err) ? next(err) : next();
      });
    }, function(err){
      return (err) ? config.callback.error({msg: err}) : config.callback.success();
    });  
  };
  
  /**
  * Import data from file
  * @param {bin} csvData
  * @param {object} user (Logged user)
  * @param {string} type
  * @param {function} callback
  */
  this.importData = function(csvData, user, type, callback) {
    csv.parse(csvData, function(err, data){
      if(err){
        __log.error(err, 'services/csvImport', 'importData');
        return callback.error('server_error');
      } else {

        // No error and file not empty
        if(data.length >= 2) {
          // Import data in order and call callback
          return csvParser.checkColumns(type, data[0], function(success, columnsNotFound){
            if(success){
              var config = {
                data          : data,
                columnsIndex  : getColumnsIndex(data.shift()),
                parseFunction : csvParser[type.toLowerCase()],
                model         : orm.models[type],
                callback      : callback
              };
              return self.import(config, user);
            } else {
              return callback.error({msg: 'columns', columnsNotFound: columnsNotFound});
            }
          });
        } else {
          return callback.error({msg: 'empty'});
        }
      }
    });
  };
  
  /**
  * Returns an array with columns positions
  * @param {array} columns
  */
  function getColumnsIndex(columns) {
    var columnsIdxs = {};
    for(var idx in columns) {
      columnsIdxs[columns[idx]] = parseInt(idx);
    }
    return columnsIdxs;
  }
  
}

module.exports = new csvImport();
