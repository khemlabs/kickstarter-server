
__ws.on('connection', function(data){
  
  var socket   = data.socket;
  var username = data.user.username || false;
  var type     = data.user.type     || false;
  
  socket.on('join-room', function(room){
    __ws.joinRoom(room, socket);
  });
  
  socket.on('leave-room', function(room){
    __ws.leaveRoom(room, socket);
  });
});