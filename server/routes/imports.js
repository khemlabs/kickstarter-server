var express   = require('express'),
    router    = express.Router(),
    fs        = require('fs'),
    path      = require('path'),
    csvImport = require(__services + 'csvImport');
    multer    = require('multer'),
    upload    = multer({ dest: __config.UPLOAD_BASE_PATH || "/space/data/app-data/uploads/" }),
    kauth     = require('khem-auth');

router.post("/users", function(req, res, next) {
  return kauth.authenticateRoles(req, res, next, 'User', 'create');
});
router.post('/users', upload.single('import'), function(req, res, next) {
  return importData(req, res, next, csvImport.users);
});

function importData(req, res, next, importFunction){
  if(!req.file || !req.file.path){
    __log.warning('Not file uploaded', 'routes/imports', 'addProducts');
    return res.status(400).send('Not file uploaded');
  }

  if (path.extname(req.file.originalname) !== '.csv') {
    return res.status(422).send('Only csv file are allowed');
  }

  fs.readFile(req.file.path, function (err, csvImportData) {
    importFunction(csvImportData, req.user, {
      success: function(response){
        unlinkFile(req.file.path);
        return res.json({ success: true, data: response || false });
      },
      error: function(err){
        __log.error(err, 'routes/imports', 'importData');
        unlinkFile(req.file.path);
        return res.status(500).send(err);
      }
    });
  });
};

function unlinkFile(file) {
  fs.unlink(file, function (err) {
    if (err) __log.error(err, 'routes/imports', 'unlinkFile');
    __log.info('File (' + file + ') removed', 'routes/imports', 'unlinkFile');
  });
}

module.exports = router;
