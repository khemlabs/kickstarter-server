var express         = require('express')
    ,router         = express.Router()
    ,orm            = require('khem-sequelize-orm')
    ,kauth          = require('khem-auth');

router.get("/login", kauth.authenticate(false));
router.post('/login', function(req, res, next) {
  orm.models.User.findOne({
    where: {username: req.body.username},
    attributes: ['user_id', 'username', 'first_name', 'last_name', 'status', 'area', 'type', 'password_hash']
  }).then(function(user){
      if(!user){
        return res.status(401).json({ success: false });
      }
      user.comparePassword(req.body.password, function(err, valid){
        if(!err && valid){
          var token = kauth.getToken({ 
              "sub": user.user_id, 
              "username": user.username,
              "type": user.type
          },'10080m'); // 1week
          orm.models.Role.findOne({
            where: {name: user.type},
            attributes: ['rules']
          }).then(function(role){
            return res.status(200).json({ 
              success: true, 
              token: token, 
              username: user.username,
              first_name: user.first_name,
              last_name: user.last_name,
              status: user.status,
              area: user.area,
              type: user.type,
              rules: role && role.rules ? role.rules : {}
            });
          });
        } else {
          return res.status(401).json({ success: false });  
        }
      });
    });
});

router.get("/logged", kauth.authenticate('*'));
router.get('/logged', function(req, res, next) {
  var user = {
    user_id: req.user.user_id,
    area: req.user.area,
    email: req.user.email,
    username: req.user.username,
    first_name: req.user.first_name,
    last_name: req.user.last_name,
    type: req.user.type
  };
  return res.status(200).json(user);
});

module.exports = router;
