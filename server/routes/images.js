var express   = require('express')
    ,router   = express.Router()
    ,multer   = require('multer')
    ,upload   = multer({ dest: __config.UPLOAD_BASE_PATH || "/data/uploads/" })
    ,kauth    = require('khem-auth');

router.get('/:id/:type?', function(req, res, next) {
  // Configure the image object, get the file and return it
  var image = require('khem-image-uploader').configure();
  // Send the image
  return image.send(req, res);
});

router.post("/", kauth.authenticate('*'));
router.post('/', upload.single('image'), function(req, res, next) {
  // If the user has uploaded a file
  if(req.file && req.file.path){    
    var file        = req.file.path;
    var path        = ( process.env.PATH_IMG || "/data/images/" ) + req.file.filename;
    var destination = path + '/raw';
    // Configure the image object, create the file and return the id
    var image = require('khem-image-uploader').configure(file, path, destination);
    //Create path, move image to path, resize image and return the id
    image.create(function(err){
      // Send the id of the image
      return image.sendID(err, req, res);
    });
  } else {
    // If no file uploaded
    return res.json({ success: false });
  }
});

module.exports = router;
