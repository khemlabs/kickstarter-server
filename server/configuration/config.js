// GLOBAL VARIABLES

global.__configuration  = __base + 'server/configuration/';
global.__middlewares    = __base + 'server/middlewares/';
global.__models         = __base + 'server/models/';
global.__routes         = __base + 'server/routes/';
global.__services       = __base + 'server/services/';
global.__websockets     = __base + 'server/websockets/';

// CONFIG VARIABLES

var config = {
  "DATABASE_NAME"     : process.env.DATABASE_NAME     || 'postgres',
  "DATABASE_USER"     : process.env.DATABASE_USER     || 'postgres',
  "DATABASE_PASSWORD" : process.env.DATABASE_PASSWORD || 'postgres', 
  "DATABASE_DIALECT"  : process.env.DATABASE_DIALECT  || "postgres",
  "DATABASE_HOST"     : process.env.DATABASE_HOST     || "db",
  "DATABASE_PORT"     : process.env.DATABASE_PORT     || 5432,
  "DATABASE_LOGGING"  : process.env.DATABASE_LOGGING  || false,
  "USE_DATABASE"      : process.env.DATABASE_NAME     || true,
  "LISTEN_PORT"       : process.env.PORT              || 3000,
  "ADMIN_USERNAME"    : process.env.ADMIN_USERNAME    || 'admin',
  "ADMIN_FIRSTNAME"   : process.env.ADMIN_FIRSTNAME   || 'admin',
  "ADMIN_LASTNAME"    : process.env.ADMIN_LASTNAME    || 'admin',
  "ADMIN_PASSWORD"    : process.env.ADMIN_PASSWORD    || 'admin',
  "ADMIN_EMAIL"       : process.env.ADMIN_EMAIL       || 'dev@khemlabs.com',
  "PATH_IMG"          : process.env.PATH_IMG          || "/data/images/",
  "PATH_LOGO"         : process.env.PATH_LOGO         || "public/assets/images/logos/khemlabslogomenu.png",
  "SECRET_PRHASE"     : process.env.SECRET_PRHASE     || Math.random().toString(36).slice(-8),
  "RES_DURATION"      : 60,
  
  "USER_ROLES" : process.env.USER_ROLES 
    ? process.env.USER_ROLES.split(',').map(function(rule){
      return rule.trim();
    }) : ['admin', 'superadmin', 'readonly'],
  
  "ADMIN_ROLES" : process.env.ADMIN_ROLES 
    ? process.env.ADMIN_ROLES.split(',').map(function(role){
      return role.trim();
    }) : ['admin', 'superadmin'],
  
  "ALLOW_ORIGIN" : process.env.ALLOW_ORIGIN 
    ? process.env.ALLOW_ORIGIN.split(',').map(function(origin){
      return origin.trim();
    }) : [],
  
  "UPLOAD_BASE_PATH"           : process.env.UPLOAD_BASE_PATH,
  "EPILOGUE_MIDDLEWARE_IGNORE" : [ 'Example.middleware.js' ],
  "SOCKET_IGNORE"              : [ 'example.js' ],
  
  "CODE_LENGTH"                : 10,
  
  "ROLES" : process.env.ROLES 
    ? process.env.ROLES.split(',').map(function(role){
      return JSON.parse(role.trim());
    }) : [{
      "name": 'superadmin',
      "rules": { 
        "User": { "list": true, "read": true, "create": true, "update": true, "delete": true },
        "Role": { "list": true, "read": true, "create": true, "update": true, "delete": true }
      }
    }, {
      "name": 'admin',
      "rules": {
        "User": { "list": true, "read": true, "create": true, "update": true, "delete": true }
      }
    }, {
      "name": 'readonly',
      "rules": {
        "User": { "list": true, "read": true }
      }
    }]
};

config.SMTP = {};

// Check for GMAIL PARAMS
if(process.env.GMAIL_SMTP_USER && process.env.GMAIL_SMTP_PASSWORD && process.env.GMAIL_SMTP_SENDER){
  config.SMTP.gmail = {
    user    : process.env.GMAIL_SMTP_USER,
    password: process.env.GMAIL_SMTP_PASSWORD,
    sender  : process.env.GMAIL_SMTP_SENDER
  };  
}

// Check for GMAIL OAUTH PARAMS
if(process.env.GMAIL_OAUTH_USERNAME && process.env.GMAIL_OAUTH_CLIENT_ID && process.env.GMAIL_OAUTH_CLIENT_SECRET && process.env.GMAIL_OAUTH_REFRESH_TOKEN && process.env.GMAIL_OAUTH_ACCESS_TOKEN && process.env.GMAIL_OAUTH_SENDER){
  config.SMTP.gmailApi = {
    username    : process.env.GMAIL_OAUTH_USERNAME,
    clientId    : process.env.GMAIL_OAUTH_CLIENT_ID,
    clientSecret: process.env.GMAIL_OAUTH_CLIENT_SECRET,
    refreshToken: process.env.GMAIL_OAUTH_REFRESH_TOKEN,
    accessToken : process.env.GMAIL_OAUTH_ACCESS_TOKEN,
    sender      : process.env.GMAIL_OAUTH_SENDER
  };
}

// Check for AMAZON OAUTH PARAMS
if(process.env.SES_ACCESS_KEY_ID && process.env.SES_SECRET_ACCES_KEY && process.env.SES_REGION && process.env.SES_SENDER){
  config.SMTP.ses = {
    accessKeyId    : process.env.SES_ACCESS_KEY_ID,
    secretAccessKey: process.env.SES_SECRET_ACCES_KEY,
    region         : process.env.SES_REGION,
    sender         : process.env.SES_SENDER
  };
}

module.exports = config;
