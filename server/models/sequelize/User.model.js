"use strict";

module.exports = function(sequelize, DataTypes) {
  var bcrypt = require('bcrypt');
  
  var Entity = sequelize.define('User', {
    user_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    username: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    first_name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    last_name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isEmail: true,
        notEmpty: true
      }
    },
    type: {
      type: DataTypes.STRING(20),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    area: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
        isIn: [['administration']]
      }
    },
    password_hash: DataTypes.STRING,
    password: {
      type: DataTypes.VIRTUAL,
      validate: {
        len: [5, 18]
      },
      set:  function(v) {
        var salt = bcrypt.genSaltSync(10);
        var hash = bcrypt.hashSync(v, salt);
        this.setDataValue('password_hash', hash);
      }
    },
    image: {
      type: DataTypes.CHAR(32),
      allowNull: true
    },
    social: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    social_type: {
      type: DataTypes.STRING,
      allowNull: true
    },
    social_id: {
      type: DataTypes.STRING,
      allowNull: true
    },
    status: {
      type: DataTypes.CHAR(8),
      allowNull: false,
      validate: {
        notEmpty: true,
        isIn: ['isactive', 'inactive']
      }
    }
  }, {
    tableName: 'user',
    indexes: [
      {name:'user_unique_username', unique:true, fields:[sequelize.fn('lower', sequelize.col('username'))]},
      {name: 'user_first_name_index', method: 'BTREE', fields: ['first_name']},
      {name: 'user_last_name_index', method: 'BTREE', fields: ['last_name']},
      {name: 'user_email_index', method: 'BTREE', fields: ['email']},
      {name: 'user_type_index', method: 'BTREE', fields: ['type']}
    ],
    classMethods: {
      
      epilogue: function(){
        var orm = require('khem-sequelize-orm');
        return {
          endpoints: ['/users', '/users/:user_id'],
          associations: true,
          search: {
            operator: '$ilike'
          },
          authenticate: true
        };
      },
      
      /**
      * Validate if the user is admin or superadmin
      * @param {string} userLoggedType
      * @param {string} userType
      * @return boolean
      */
      validateUserType: function (userLoggedType, userType) {
        return (
          (userLoggedType == 'superadmin')
          ||
          (userLoggedType == 'admin' && (userType != 'superadmin' || userType != 'admin'))
        );
      },

      /**
      * Validate if the user is admin or superadmin
      */
      checkType: function(req, res, context) {
        var instanceTYPE = context.instance ? context.instance.get('type') : null;
        var type = req.body.type || instanceTYPE;
        if(Entity.validateUserType(req.user.type), type){
          return context.continue();
        } else {
          return res.status(401).json({ success: false });        
        }
      },

      /**
      * Import user and dependencies
      * @param {object} data {[user: {object}}
      * @param {function} callback
      */
      import: function(data, callback) {
        Entity.createEntity(data.user, function(err, resultset){
          return callback(err, resultset);
        });
      },

      /**
      * Create entity
      * @param {object} data
      * @param {function} callback
      */
      createEntity: function(data, callback) {
        Entity.findOrCreate({where: {username: data.username}, defaults: data})
          .then(function(resultset, created){
            Entity.findOne({where: {username: data.username}}).then(function(entity){
              return callback(null, entity);
            });
        }).catch(sequelize.ValidationError, function (err) {
          __log.error(err, 'models/sequelize/user', 'create');
          return callback(err);
        }).catch(function (err) {
          __log.error(err, 'models/sequelize/user', 'create');
          return callback(err.message);
        });
      },

      /**
      * Check if user exist and validate if system user is admin
      */
      checkUserBeforeDelete: function(req, res, context) {
        var orm = require('khem-sequelize-orm');
        orm.models.User.findOne({user_id: req.body.user_id}).then(function(user){
          if(user){
            return Entity.checkType(req, res, context);
          } else {
            return res.status(404).json({ success: false });
          }
        }).catch(orm.sequelize.ValidationError, function (err) {
          __log.error(err, 'models/sequelize/user', 'delete', req);
          return res.status(500).send(err);
        }).catch(function (err) {
          __log.error(err, 'models/sequelize/user', 'delete', req);
          return res.status(500).send(err);
        });
      }
      
    },
    instanceMethods: {
      /**
      * Compare password with password hash
      * @param {String} psw
      */
      comparePassword: function(psw, callback) {
        bcrypt.compare(psw, this.password_hash, function (err, isMatch) {
          if (err) {
            return callback(err);
          }
          callback(null, isMatch);
        });
      }
    }
  });

  return Entity;
};
