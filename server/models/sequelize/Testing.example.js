"use strict";

/**
* This model serves as a representation of an object capable of being 
* used by the kickstarter to automatically generate files and routes.
*
* @WARNING: IF YOU WANT TO AUTO GENERATE FILES, MODEL NAME HAS TO BE ALL IN LOWERCASE WITH FIRST LETTER CAPITALIZED
*
* Generate fronted files from models
* ----------------------------------
* To generate fronted files run in command line node scripts/generate-from-models.js,
* this will use the method filesGenerator() to get the data.
*
* Create routes from models
* -------------------------
* The kickstarter uses the method epilogue for generating the routes.
*
* RUN THIS TEST
* -------------
* If you want to test the generators rename this model to Testing.model.js
* Warning: If you do this, the table "testing" will be created in your database.
*/
module.exports = function(sequelize, DataTypes) {  
  /**
  * Definition of the entity for sequelize
  * @docs http://docs.sequelizejs.com/en/latest/
  */
  var Entity = sequelize.define('Testing', {
    testing_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    status: {
      type: DataTypes.CHAR(8),
      allowNull: false,
      defaultValue: 'active',
      validate: {
        notEmpty: true,
        isIn: [['isactive', 'inactive']]
      }
    }
  }, {
    tableName: 'testing',
    classMethods: {
      
      filesGenerator: function(){
        
        return {
          delete: true,
          compile: true,
          title: 'Testing',
          modelname: 'Testing',
          plural: 'Testings',
          key: 'description',
          keyname: 'Descripción',
          id: 'testing_id',
          columns: [
            {
              key: 'status', 
              name: 'Estado', 
              required: false, 
              unique: false, 
              type: 'select',
              default: 'active',
              list: [
                {key: 'Activo', value: 'active'},
                {key: 'Inactivo', value: 'inactive'}
              ]
            },
          ]
        };
      },
      
      epilogue: function(){
        /**
        * This method return the data needed by epilogue for routes generation
        * @param authenticate {false || array} Indicates if endpoints needs authentication
        * @docs https://github.com/dchester/epilogue
        * @warning This will include the middleware middlewares/associations. The query 
        *          generated will fail, if the same model is required by two or more models.
        *          The model has to have only one primery numeric key and it has to be the 
        *          foreign key of the association.
        *          If you want to disable inclusion use includeMiddleware=false.
        */
        var orm = require('khem-sequelize-orm');
        return {
          endpoints: ['/testings', '/testings/:testing_id'],
          search: {
            operator: '$ilike'
          },
          associations: true,
          includeMiddleware: true,
          //authenticate: __config.ADMIN_ROLES
          //authenticate: true
          authenticate: false
        };
      }
    }
  });

  return Entity;
};
