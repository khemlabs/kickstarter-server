"use strict";

module.exports = function(sequelize, DataTypes) {
  var bcrypt = require('bcrypt');
  
  var Entity = sequelize.define('Role', {
    role_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(20),
      allowNull: false,
      unique: true,
      validate: {
        notEmpty: true
      }
    },
    rules: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notEmpty: true
      },
      set: function(v) {
        this.setDataValue('rules', JSON.stringify(v));
      },
      get: function(v) {
        return JSON.parse(this.getDataValue(v));
      }
    }
  }, {
    tableName: 'roles',
    indexes: [
      {name:'roles_unique_name', unique:true, fields:[sequelize.fn('lower', sequelize.col('name'))]},
      {name: 'roles_name_index', method: 'BTREE', fields: ['name']}
    ],
    classMethods: {
      
      epilogue: function(){
        var orm = require('khem-sequelize-orm');
        return {
          endpoints: ['/roles', '/roles/:role_id'],
          associations: true,
          search: {
            operator: '$ilike'
          },
          authenticate: true
        };
      }
      
    }
  });

  return Entity;
};
