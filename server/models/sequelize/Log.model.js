"use strict";

module.exports = function(sequelize, DataTypes) {

  var Entity = sequelize.define('Log', {
    log_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    action: {
      type: DataTypes.CHAR(6),
      allowNull: false,
      validate: {
        notEmpty: true,
        isIn: [['delrec', 'delete', 'update', 'insert']]
      }
    },
    entity: {
      type: DataTypes.STRING(20),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    ip: {
      type: DataTypes.STRING(20),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    data: {
      type: DataTypes.JSON,
      allowNull: true
    },
    user: {
      type: DataTypes.JSON,
      allowNull: true
    }
  }, {
    tableName: 'log',
    
    classMethods: {      
      epilogue: function(){ 
        return false;
      },
      
      /**
      * Log action
      */
      log: function(data) {
        return new Promise(function (resolve) {
          if(data && data.type && data.action && data.entity && data.req){
            var orm               = require(__models + 'orm');
            var getIP             = require('ipware')().get_ip;
            var search            = {where: {}};
            var id_key            = data.type.toLowerCase() + '_id';
            search.where[id_key]  = data.entity[id_key];
            if(search.where[id_key]){
              if(data.include){
                search.include  = data.include;
              }
              if(data.attributes){
                search.attributes = data.attributes;
              }
              orm.models[data.type].findOne(search).then(function(entity){
                if(entity){
                  var user = {};
                  if(data.req.user){
                    user.username   = data.req.user.username;
                    user.email      = data.req.user.email;
                  } else {
                    user.error      = 'unauthenticated-method';
                  }
                  var values = {
                    action  : data.action,
                    entity  : data.type,          
                    ip      : getIP(data.req).clientIp.substring(0, 20),
                    data    : entity,
                    user    : user
                  };
                  orm.models.Log.create(values, function(result){}, function(err){
                    __log.error(err, 'server/models/sequelize/log', 'log');
                  });
                } else {
                  __log.error('Entity (' + data.type + ') with id (' + data.entity[id_key] + ') not found', 'server/models/sequelize/log', 'log');  
                }
                if(resolve) return resolve();
              }, function(err){
                __log.error(err, 'server/models/sequelize/log', 'log');
                if(resolve) return resolve();
              });  
            } else {
              __log.error('Entity (' + data.type + ') without id', 'server/models/sequelize/log', 'log');  
              if(resolve) return resolve();
            }  
          } else {
            __log.error('Invalid params', 'server/models/sequelize/log', 'log');
            if(resolve) return resolve();
          }  
        });
      }
    }
  });

  return Entity;
};
