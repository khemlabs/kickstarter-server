"use strict";

var orm = require('khem-sequelize-orm');

module.exports = {
  create: { 
    write: {
      before: function(req, res, context) {
        if(req.body.name == 'superadmin' || req.body.name == 'admin' || req.body.name == 'readonly')
          return res.status(500).send(error);
        else
          return context.continue();
      }
    } 
  },
  
  update: { 
    write: {
      before: function(req, res, context) {
        if(req.body.name == 'superadmin' || req.body.name == 'admin' || req.body.name == 'readonly')
          return res.status(500).send(error);
        else
          return context.continue();
      }
    } 
  },
  
  delete: {
    write: {
      before: function(req, res, context) {
        if(req.body.name == 'superadmin' || req.body.name == 'admin' || req.body.name == 'readonly')
          return res.status(500).send(error);
        else
          return context.continue();
      }
    }
  }
};