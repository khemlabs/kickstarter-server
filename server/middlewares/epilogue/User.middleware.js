"use strict";

var orm = require('khem-sequelize-orm');

var attributes4log = ['user_id', 'username', 'first_name', 'last_name', 'email', 'type', 'area'];

/**
* Get write object depending on method
*/
var getWriteObject = function(method){
  return {
    before: function(req, res, context) {
      // Check if user logged is admin, on succes context.continue()
      return orm.models.User.checkType(req, res, context);
    }, 
    after:  function(req, res, context) {
      // Log insert user action
      orm.models.Log.log({req: req, action: method, type: 'User', attributes: attributes4log, entity: context.instance});
      return context.continue();
    }
  }; 
};

module.exports = {
  create: { write: getWriteObject('insert') },
  
  update: { write: getWriteObject('update') },
  
  delete: {
    write: {
      before: function(req, res, context) {
        // Log delete user action
        orm.models.Log.log({
          req: req, 
          action: 'delete', 
          type: 'User', 
          attributes: attributes4log, 
          entity: context.instance
        }).then(function(){
          // Check if user logged is admin and that user to delete exist, on succes context.continue()
          return orm.models.User.checkUserBeforeDelete(req, res, context);  
        });
      }
    }
  }
};