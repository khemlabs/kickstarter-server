"use strict";

var kauth = require('khem-auth');

/**
* This file respects the format of a middleware for "epilogue",
* allowing to insert functionality, before, during and 
* after the ejecucción of "epilogue".
* 
* The name of the middleware has to be the same as the model.
* Example: 
* server/models/sequelize/Example.model.js
* server/middlewares/epilogue/Example.middleware.js
*
* For more information go to https://github.com/dchester/epilogue
*/
module.exports = {
  
  create: {
    start: function(req, res, context) {
      return context.continue();
    },
    auth: function(req, res, context) {
      return kauth.authenticateUnited(req, res, context.continue, __conf.ADMIN_ROLES);
    },
    data: function(req, res, context) {
      return context.continue();
    },
    send: function(req, res, context) {
      return context.continue();
    },
    complete: function(req, res, context) {
      return context.continue();
    },
    write: {
      before: function(req, res, context) {
        // modify data before writing list data
        return context.continue();
      },
      action: function(req, res, context) {
        // change behavior of actually writing the data
        return context.continue();
      },
      after: function(req, res, context) {
        // set some sort of flag after writing list data
        return context.continue();
      }
    }
  },
  
  list: {
    /**
    * Example usign autocompleteService
    */
    start: function(req, res, context) {
      require(__services + 'autocompleteService').get(
        models.Example, 
        'example_id',
        'description',
        false,
        ['example_id', 'description'],
        req, 
        []
      ).then(function(results){ 
        return res.status(200).json(results);
      }, function(){
        return context.continue();  
      });  
    },
    auth: function(req, res, context) {
      return kauth.authenticateUnited(req, res, context.continue, __conf.ADMIN_ROLES);
    },
    data: function(req, res, context) {
      return context.continue();
    },
    send: function(req, res, context) {
      return context.continue();
    },
    complete: function(req, res, context) {
      return context.continue();
    },
    fetch: {
      before: function(req, res, context) {
        // modify data before writing list data
        return context.continue();
      },
      action: function(req, res, context) {
        // change behavior of actually writing the data
        return context.continue();
      },
      after: function(req, res, context) {
        // set some sort of flag after writing list data
        return context.continue();
      }
    }
  },
  
  read: {
    start: function(req, res, context) {
      return context.continue();
    },
    auth: function(req, res, context) {
      return kauth.authenticateUnited(req, res, context.continue, __conf.ADMIN_ROLES);
    },
    data: function(req, res, context) {
      return context.continue();
    },
    send: function(req, res, context) {
      return context.continue();
    },
    complete: function(req, res, context) {
      return context.continue();
    },
    fetch: {
      before: function(req, res, context) {
        // modify data before writing list data
        return context.continue();
      },
      action: function(req, res, context) {
        // change behavior of actually writing the data
        return context.continue();
      },
      after: function(req, res, context) {
        // set some sort of flag after writing list data
        return context.continue();
      }
    }
  },
  
  update: {
    start: function(req, res, context) {
      return context.continue();
    },
    auth: function(req, res, context) {
      return kauth.authenticateUnited(req, res, context.continue, __conf.ADMIN_ROLES);
    },
    data: function(req, res, context) {
      return context.continue();
    },
    send: function(req, res, context) {
      return context.continue();
    },
    complete: function(req, res, context) {
      return context.continue();
    },
    write: {
      before: function(req, res, context) {
        // modify data before writing list data
        return context.continue();
      },
      action: function(req, res, context) {
        // change behavior of actually writing the data
        return context.continue();
      },
      after: function(req, res, context) {
        // set some sort of flag after writing list data
        return context.continue();
      }
    }
  },
  
  delete: {
    start: function(req, res, context) {
      return context.continue();
    },
    auth: function(req, res, context) {
      return kauth.authenticateUnited(req, res, context.continue, __conf.ADMIN_ROLES);
    },
    data: function(req, res, context) {
      return context.continue();
    },
    send: function(req, res, context) {
      return context.continue();
    },
    complete: function(req, res, context) {
      return context.continue();
    },
    write: {
      before: function(req, res, context) {
        // modify data before writing list data
        return context.continue();
      },
      action: function(req, res, context) {
        // change behavior of actually writing the data
        return context.continue();
      },
      after: function(req, res, context) {
        // set some sort of flag after writing list data
        return context.continue();
      }
    }
  }
};