############################################################
# Dockerfile to build NodeJS 4 Installed Containers
# Based on Node:4.4.3
############################################################

FROM khemlabs/kickstarter-server-base

# Copy application folder and configurations
COPY . /app

# Create data directory
WORKDIR /app

# NPM INSTALL

RUN npm install

CMD ["forever", "-w", "--minUptime", "1000", "--spinSleepTime", "1000", "app.js"]
