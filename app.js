// Create and configure server
require('./basevars');

var express         = require('express')
    ,bodyParser     = require('body-parser')
    ,app            = express()
    ,path           = require('path')
    ,http           = require('http')
    ,kauth          = require('khem-auth')
    ,kcross         = require('khem-cross-origin')(__config.ALLOW_ORIGIN)
    ,orm            = require('khem-sequelize-orm')
    ,epilogue       = require('khem-epilogue')
    ,usersRouter    = require(__routes   + 'users')
    ,importsRouter  = require(__routes   + 'imports')
    ,imagesRouter   = require(__routes   + 'images')
    ,databaseInit   = require(__services + 'databaseInitialization');

var server = http.createServer(app);

// Build Sockets - Uncomment for websockets
//var ss = require('khem-socket');
//global.__ws = new ss()
//__ws.configure( server, __config.SOCKET_IGNORE, __config.SECRET_PRHASE );

// Configure app
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));
app.set('permission', {role: 'type'});

// ***** ORM and Endpoints generator *****

app.use(kauth.configure(__config.SECRET_PRHASE, orm.models.User, orm.models.Role));

app.use(kcross.parse);

// Routes
app.use('/users/', usersRouter);
app.use('/imports', importsRouter);
app.use('/images', imagesRouter);

epilogue.initialize( app, orm.database, __config.EPILOGUE_MIDDLEWARE_IGNORE );

// ***** Sync sequelize and init server *****
orm.database.sync({ force: orm.sequelize.forceSync }).then(function() {
  // Init database
  databaseInit.init().then(function(){
    // Init server
    server.listen(__config.LISTEN_PORT, function() {
      var address = server.address().address;
      var host = address && address !== '::' ? address : 'localhost',
          port = server.address().port;
      console.info('[app] Listening at http://%s:%s', host, port);  
    });
  });
});

module.exports = app;
