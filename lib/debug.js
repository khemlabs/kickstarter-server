// Create and configure server
require( '../basevars');

var spawn = require('child_process').spawn;

var debug = function() {  
  var self = this;
  
  this.env = {};
  this.path = __base + 'lib/debug.sh';
  
  this.init = function() {
    if(process.env.CONTAINER_NAME) self.env.CONTAINER_NAME = process.env.CONTAINER_NAME
    
    var command = spawn( 'bash', [self.path], self.env );
    
    command.stdout.on('data', function (data) {
      console.log(data.toString());
    });

    command.stderr.on('data', function (data) {
      console.log(data.toString());
    });

    command.on('exit', function (code) {
      if(code.toString() != '0')
        console.log('Bash finished with errors');
      else 
        console.log('Bash finished without errors');
    });
  }
  
  this.init();
};

module.exports = new debug();
