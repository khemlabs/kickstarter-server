#!/bin/bash

echo '********** INIT lib/local.sh **********'

NPM_PATH=$(pwd)/node_modules/
echo 'Searching for path '$NPM_PATH

echo '********** Check if node-inspector exist **********'
if npm list -g | grep 'node-inspector'; then
  echo "Node inspector is already installed"
else
  echo "Node inspector is not installed"
  npm install -g node-inspector
fi

echo '********** Check if node_modules exist **********'
if [ -d "$NPM_PATH" ]; then
  echo 'node_modules exist'
else
  echo 'node_modules does not exist, execute "npm install"'
  npm install
fi

echo '********** RUN SERVER **********'
forever -w --minUptime 1000 --spinSleepTime 1000 -c 'node --debug' app.js