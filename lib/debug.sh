#!/bin/bash

# Khemlabs - Debug script #

### What is this script for? ###

# This script starts a docker container for debugging a node js docker app
# for this script to work is necessary that the docker containing the app
# has node-inspector installed, share his volume and port forward 3000 and 9080

bold=`tput bold`
normal=`tput sgr0`

# DO NOT MODIFY

if [ -z "${CONTAINER_NAME+xxx}" ]; 
then 
  printf "\n${bold}==> CONTAINER_NAME not set using kickstarter-server-0.2.1 instead\n${normal}"; 
fi
CONTAINER_NAME=${CONTAINER_NAME:-"kickstarter-server-0.2.1"}

SUCCESS=$(docker inspect -f {{.State.Running}} $CONTAINER_NAME 2> /dev/null)
printf "\n${bold}==> Cheking if $CONTAINER_NAME is active...\n${normal}"

if [ -z $SUCCESS ];
then
  printf "${bold}==> Container $CONTAINER_NAME is not running${normal}\n"
else
  printf "Container $CONTAINER_NAME active \n\n"
  printf "${bold}==> Cheking if previous node-inspector...\n${normal}"
  SUCCESS=$(docker inspect -f {{.State.Running}} node-inspector 2> /dev/null)
  if [ -z $SUCCESS ];
  then
    printf "No previous container found \n"
  else
    printf "Previous container found \n"
    printf "${bold}==> Stopping previous node-inspector...${normal} \n"
    docker stop node-inspector
    printf "${bold}==> Removing previous node-inspector...${normal} \n"
    docker rm -v node-inspector
  fi
  printf "${bold}==> Starting new node-inspector for container %s...${normal} \n" "$CONTAINER_NAME"
  docker run --name node-inspector --net=container:$CONTAINER_NAME --volumes-from $CONTAINER_NAME -d khemlabs/node-inspector
  printf "${bold}==> Debug available on chrome browser at http://127.0.0.1:9080/?ws=127.0.0.1:9080&port=5858${normal}\n"
fi